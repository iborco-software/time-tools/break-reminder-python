# Build MacOS application icon

Use [icns-creator](https://github.com/jamf/icns-Creator) to (re)generate the *app.icns*:

```bash
bash icns_creator.sh app-icon-default.png app
```
