import os
import subprocess
from pathlib import Path
from typing import Final

from PySide2.QtWidgets import QCheckBox, QDialog, QDialogButtonBox, QPushButton, QVBoxLayout

from breakreminder.breaks_widget import BreaksWidget
from breakreminder.settings import Settings


class PreferencesDialog(QDialog):
    APP_PATH: Final[Path] = (Path(__file__).parent.parent.parent / 'dist' / 'Break Reminder.app').absolute()

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.setWindowTitle('Preferences')

        self.normalBreaks = BreaksWidget(
            title='Normal Breaks',
            info="Take these every hour or so. Break duration is increased automatically if you don't take it when scheduled.",
            break_values=Settings.Breaks.NORMAL_BREAK_VALUES,
            work_values=Settings.Breaks.NORMAL_WORK_VALUES
        )

        self.miniBreaks = BreaksWidget(
            title='Mini Breaks',
            info="Mini breaks give you a chance to stretch and relax. They only show a reminder and are not increased if you don't take them when scheduled.",
            break_values=Settings.Breaks.MINI_BREAK_VALUES,
            work_values=Settings.Breaks.MINI_WORK_VALUES
        )
        self.miniBreaks.setCheckable(True)

        saveButton = QPushButton(self.tr('&Save'))
        saveButton.setDefault(True)
        saveButton.clicked.connect(self.accept)

        buttonBox = QDialogButtonBox(self)
        buttonBox.addButton(saveButton, QDialogButtonBox.AcceptRole)

        self.trackOnStartCheckBox = QCheckBox(self.tr('Start tracking when starting the app'))

        self.startAtLoginCheckBox = QCheckBox(self.tr('Start the app when you login'))
        self.startAtLoginCheckBox.setEnabled(self.APP_PATH.exists())
        self.updateStartAtLoginCheckbox()
        self.startAtLoginCheckBox.setVisible(self.__canRunLoginItems)
        self.startAtLoginCheckBox.toggled.connect(self.onStartAtLoginCheckBoxToggled)

        layout = QVBoxLayout()
        layout.addWidget(self.normalBreaks)
        layout.addWidget(self.miniBreaks)
        layout.addWidget(self.trackOnStartCheckBox)
        layout.addWidget(self.startAtLoginCheckBox)
        layout.addStretch(0)
        layout.setStretch(4, 1)
        layout.addWidget(buttonBox)
        self.setLayout(layout)

    def accept(self):
        self.writeBreaksSettings()
        super().accept()

    def done(self, arg):
        self.writeGeometrySettings()
        super().done(arg)

    def readSettings(self) -> None:
        settings = Settings()
        print(f'reading settings from: {settings.fileName}')

        self.restoreGeometry(settings.preferencesDialog.geometry)

        self.normalBreaks.breakSlider.value = settings.breaks.normalBreak
        self.normalBreaks.workSlider.value = settings.breaks.normalWork

        self.miniBreaks.setChecked(settings.breaks.useMiniBreak)
        self.miniBreaks.breakSlider.value = settings.breaks.miniBreak
        self.miniBreaks.workSlider.value = settings.breaks.miniWork

        self.trackOnStartCheckBox.setChecked(settings.app.trackOnStart)

    def writeGeometrySettings(self):
        settings = Settings()

        settings.preferencesDialog.geometry = self.saveGeometry()

    def writeBreaksSettings(self):
        settings = Settings()

        settings.breaks.normalBreak = self.normalBreaks.breakSlider.value
        settings.breaks.normalWork = self.normalBreaks.workSlider.value

        settings.breaks.useMiniBreak = self.miniBreaks.isChecked()
        settings.breaks.miniBreak = self.miniBreaks.breakSlider.value
        settings.breaks.miniWork = self.miniBreaks.workSlider.value

        settings.app.trackOnStart = self.trackOnStartCheckBox.isChecked()

    def fixedEnvironment(self):
        my_env = os.environ
        if 'RESOURCEPATH' in my_env:
            os.environ['PATH'] = f"{my_env['RESOURCEPATH']}:{my_env.get('PATH', '')}"
        return my_env

    def isStartingAtLogin(self) -> bool:
        try:
            s = subprocess.run(['login-items', 'paths'], capture_output=True, env=self.fixedEnvironment())
            items = set(s.stdout.split(b'\n'))
            items.discard(b'')
            is_starting = str(self.APP_PATH).encode() in items
            print('>>> is starting at login:', is_starting)
            self.__canRunLoginItems = True
            return is_starting
        except:
            self.__canRunLoginItems = False
            return False

    def onStartAtLoginCheckBoxToggled(self, value: bool) -> None:
        try:
            if value:
                subprocess.run(['login-items', 'add', str(self.APP_PATH)], env=self.fixedEnvironment())
            else:
                subprocess.run(['login-items', 'rm', self.APP_PATH.stem], env=self.fixedEnvironment())
            self.__canRunLoginItems = True
        except:
            self.__canRunLoginItems = False

    def updateStartAtLoginCheckbox(self) -> None:
        self.startAtLoginCheckBox.setChecked(self.isStartingAtLogin())

if __name__ == '__main__':
    from PySide2.QtWidgets import QApplication

    Settings.init()

    _ = QApplication([])

    dialog = PreferencesDialog()
    dialog.readSettings()
    dialog.show()
    success = dialog.exec()
    print('success:', success)
