from typing import Final, List

from PySide2.QtCore import QByteArray, QCoreApplication, QSettings


class Settings:
    ORGANIZATION_NAME: Final[str] = "Ioan Calin"
    ORGANIZATION_DOMAIN: Final[str] = "borco.ro"
    APPLICATION_NAME: Final[str] = "Break Reminder"

    @classmethod
    def init(cls) -> None:
        QSettings.setDefaultFormat(QSettings.IniFormat)
        QCoreApplication.setOrganizationName(cls.ORGANIZATION_NAME)
        QCoreApplication.setOrganizationDomain(cls.ORGANIZATION_DOMAIN)
        QCoreApplication.setApplicationName(cls.APPLICATION_NAME)


    class PreferencesDialog:
        GROUP: Final[str] = 'preferences.dialog'
        GEOMETRY_KEY: Final[str] = 'geometry'

        def __init__(self, settings: QSettings):
            self.__s = settings

        @property
        def geometry(self) -> QByteArray:
            return QByteArray(self.__s.value(f'{self.GROUP}/{self.GEOMETRY_KEY}'))

        @geometry.setter
        def geometry(self, value: QByteArray) -> None:
            self.__s.setValue(f'{self.GROUP}/{self.GEOMETRY_KEY}', value)


    class Breaks:
        GROUP: Final[str] = 'breaks'

        NORMAL_BREAK_KEY: Final[str] = 'normal_break'
        NORMAL_BREAK_DEFAULT: Final[int] = 4 * 60
        NORMAL_BREAK_VALUES: Final[List[int]] = [10, 20, 30, 40, 50] + list(map((60).__mul__, range(1, 61)))

        NORMAL_WORK_KEY: Final[str] = 'normal_work'
        NORMAL_WORK_DEFAULT: Final[int] = 50 * 60
        NORMAL_WORK_VALUES: Final[List[int]] = [10, 20, 30, 40, 50, 60, 120, 180, 240] + list(map((60).__mul__, range(5, 4 * 60 + 1, 5)))

        USE_MINI_BREAK_KEY: Final[str] = 'use_mini_break'
        USE_MINI_BREAK_DEFAULT: Final[bool]= True

        MINI_BREAK_KEY: Final[str] = 'mini_break'
        MINI_BREAK_DEFAULT: Final[int] = 30
        MINI_BREAK_VALUES: Final[List[int]] = [5, 10, 15, 20, 30, 40, 50] + list(map((30).__mul__, range(2, 21)))

        MINI_WORK_KEY: Final[str] = 'mini_work'
        MINI_WORK_DEFAULT: Final[int] = 10
        MINI_WORK_VALUES: Final[List[int]] = [10, 15, 20, 30] + list(map((60).__mul__, range(1, 31)))

        def __init__(self, settings: QSettings):
            self.__s = settings

        @property
        def normalBreak(self) -> int:
            return self.__s.value(f'{self.GROUP}/{self.NORMAL_BREAK_KEY}', self.NORMAL_BREAK_DEFAULT, type=int)

        @normalBreak.setter
        def normalBreak(self, value: int) -> None:
            self.__s.setValue(f'{self.GROUP}/{self.NORMAL_BREAK_KEY}', value)

        @property
        def normalWork(self) -> int:
            return self.__s.value(f'{self.GROUP}/{self.NORMAL_WORK_KEY}', self.NORMAL_WORK_DEFAULT, type=int)

        @normalWork.setter
        def normalWork(self, value: int) -> None:
            self.__s.setValue(f'{self.GROUP}/{self.NORMAL_WORK_KEY}', value)

        @property
        def useMiniBreak(self) -> bool:
            return self.__s.value(f'{self.GROUP}/{self.USE_MINI_BREAK_KEY}', self.USE_MINI_BREAK_DEFAULT, type=bool)

        @useMiniBreak.setter
        def useMiniBreak(self, value: bool) -> None:
            self.__s.setValue(f'{self.GROUP}/{self.USE_MINI_BREAK_KEY}', value)

        @property
        def miniBreak(self) -> int:
            return self.__s.value(f'{self.GROUP}/{self.MINI_BREAK_KEY}', self.MINI_BREAK_DEFAULT, type=int)

        @miniBreak.setter
        def miniBreak(self, value: int) -> None:
            self.__s.setValue(f'{self.GROUP}/{self.MINI_BREAK_KEY}', value)

        @property
        def miniWork(self) -> int:
            return self.__s.value(f'{self.GROUP}/{self.MINI_WORK_KEY}', self.MINI_WORK_DEFAULT, type=int)

        @miniWork.setter
        def miniWork(self, value: int) -> None:
            self.__s.setValue(f'{self.GROUP}/{self.MINI_WORK_KEY}', value)


    class App:
        GROUP: Final[str] = 'breaks'

        TRACK_ON_START_KEY: Final[str] = 'track_on_start'
        TRACK_ON_START_DEFAULT: Final[bool] = False

        def __init__(self, settings: QSettings):
            self.__s = settings

        @property
        def trackOnStart(self) -> bool:
            return self.__s.value(f'{self.GROUP}/{self.TRACK_ON_START_KEY}', self.TRACK_ON_START_DEFAULT, type=bool)

        @trackOnStart.setter
        def trackOnStart(self, value: bool) -> None:
            self.__s.setValue(f'{self.GROUP}/{self.TRACK_ON_START_KEY}', value)


    def __init__(self):
        self.__s = QSettings()
        self.__preferencesDialog = Settings.PreferencesDialog(self.__s)
        self.__breaks = Settings.Breaks(self.__s)
        self.__app = Settings.App(self.__s)

    @property
    def app(self) -> App:
        return self.__app

    @property
    def breaks(self) -> Breaks:
        return self.__breaks

    @property
    def fileName(self) -> str:
        return self.__s.fileName()

    @property
    def preferencesDialog(self) -> PreferencesDialog:
        return self.__preferencesDialog


if __name__ == '__main__':
    Settings.init()

    s = Settings()
    print('>>> fileName:', s.fileName)
    print('>>> breaks.normalWork type: ', type(s.breaks.normalWork))
    print('>>> breaks.normalWork value:', s.breaks.normalWork)
    print('>>> breaks.useMiniBreak type: ', type(s.breaks.useMiniBreak))
    print('>>> breaks.useMiniBreak value:', s.breaks.useMiniBreak)
    print('>>> preferencesDialog.geometry type: ', type(s.preferencesDialog.geometry))
    print('>>> preferencesDialog.geometry value:', s.preferencesDialog.geometry)
