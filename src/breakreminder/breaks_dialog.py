from datetime import timedelta

from humanize import precisedelta
from PySide2.QtCore import Qt
from PySide2.QtWidgets import QDialog, QDialogButtonBox, QLabel, QProgressBar, QPushButton, QVBoxLayout


class BreaksDialog(QDialog):
    def __init__(self, next_break_duration: int) -> None:
        super().__init__()
        self.setWindowFlag(Qt.FramelessWindowHint, True)

        self.__nextBreakDuration = next_break_duration
        self.button = QPushButton(self.tr('&Skip'))
        self.button.setDefault(True)
        self.button.clicked.connect(self.accept)

        buttonBox = QDialogButtonBox()
        buttonBox.addButton(self.button, QDialogButtonBox.AcceptRole)

        self.breakLabel = QLabel()
        self.breakLabel.setAlignment(Qt.AlignHCenter)
        self.breakProgressBar = QProgressBar()
        self.breakProgressBar.setMaximum(self.__nextBreakDuration)

        layout = QVBoxLayout()
        layout.addStretch()
        layout.addWidget(self.breakLabel)
        layout.addWidget(self.breakProgressBar)
        layout.addStretch()
        layout.addWidget(buttonBox)
        self.setLayout(layout)

        self.setMinimumSize(640, 300)

        self.onTimerChanged(self.__nextBreakDuration)

    def onTimerChanged(self, timer: int) -> None:
        self.breakLabel.setText(self.humanizedDuration(timer))
        self.breakProgressBar.setValue(timer)
        if timer <= 0:
            self.button.setText('Done')

    def humanizedDuration(self, seconds: int) -> str:
        if seconds > 0:
            return str(precisedelta(timedelta(seconds=seconds)))
        elif seconds < 0:
            return 'On break for ' + str(precisedelta(timedelta(seconds=-seconds + self.__nextBreakDuration)))
        else:
            return ''


if __name__ == '__main__':
    from PySide2.QtCore import QTimer
    from PySide2.QtWidgets import QApplication

    from breakreminder.history import History

    _ = QApplication([])

    elapsed: int = 0

    def onTimeout():
        global elapsed
        elapsed += 1
        history.updateLastDuration(elapsed // 10)

    history = History()

    history.miniBreakDuration = 5
    history.miniWorkDuration = 90
    history.normalBreakDuration = 20
    history.normalWorkDuration = 180

    history.useMiniBreak = True

    items = [(True, 10), (False, 0)]

    for is_work, duration in items:
        if is_work:
            history.startWork()
        else:
            history.startBreak()
        history.updateLastDuration(duration)

    timer = QTimer()
    timer.timeout.connect(onTimeout)
    timer.start(100)

    dialog = BreaksDialog(history.nextBreakDuration)
    history.timerChanged.connect(dialog.onTimerChanged)
    dialog.show()

    success = dialog.exec()

    if success == 1 or history.timer < 0:
        history.finishBreak()
    else:
        history.postponeBreak()

    print('>>> success:', success)
    print('>>> timer:', history.timer)
    print('>>> timer:', history)
