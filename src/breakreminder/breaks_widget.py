from typing import List

from PySide2.QtWidgets import QCheckBox, QGroupBox, QSizePolicy, QVBoxLayout, QLabel

from breakreminder.time_slider_widget import TimeSliderWidget


class BreaksWidget(QGroupBox):
    def __init__(self,
                title: str = 'Some Break',
                info: str = 'Breaks are good',
                break_prefix: str = 'Break for',
                break_values: List[int] = list(map((10).__mul__, range(1, 6))) + list(map((30).__mul__, range(2, 21))),
                work_prefix = 'Every',
                work_values: List[int] = list(map((5 * 60).__mul__, range(1, 13))),
                **kwargs) -> None:
        super().__init__(**kwargs)
        self.setTitle(title)

        size_policy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.MinimumExpanding)
        self.setSizePolicy(size_policy)

        self.info = QLabel(self)
        self.info.setText(info)
        self.info.setWordWrap(True)

        self.breakSlider = TimeSliderWidget(break_prefix)
        self.breakSlider.values = break_values
        self.workSlider = TimeSliderWidget(work_prefix)
        self.workSlider.values = work_values

        layout = QVBoxLayout()
        layout.addWidget(self.info)
        layout.addWidget(self.breakSlider)
        layout.addWidget(self.workSlider)
        self.setLayout(layout)


if __name__ == '__main__':
    from PySide2.QtWidgets import QApplication, QDialog

    app = QApplication([])
    window = QDialog()

    bw1 = BreaksWidget(title='Breaks')

    bw2 = BreaksWidget(title='Mini Breaks')
    bw2.setCheckable(True)

    layout = QVBoxLayout()
    layout.addWidget(bw1)
    layout.addWidget(bw2)
    layout.addStretch()
    window.setLayout(layout)

    window.show()
    app.exec_()
