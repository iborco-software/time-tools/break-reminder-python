from datetime import timedelta

from humanize import precisedelta


def humanizedDuration(seconds: int):
    if seconds > 0:
        return str(precisedelta(timedelta(seconds=seconds)))
    else:
        return '0 seconds'

def compactDuration(seconds: int):
    multiplier = 1 if seconds >= 0 else -1
    seconds *= multiplier

    mins = (seconds // 60)
    secs = seconds % 60

    ret = f'{mins}:{str(secs).zfill(2)}'
    if multiplier < 0:
        ret = '-' + ret
    return ret.rjust(6)
