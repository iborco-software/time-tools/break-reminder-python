from typing import List

from PySide2.QtCore import QObject, Signal


class History(QObject):

    timerChanged = Signal(int)
    startMiniBreak = Signal(int)

    class Item:
        def __init__(self, duration: int, isWork: bool) -> None:
            self.duration = duration
            self.isWork = isWork

        def __repr__(self):
            return f'{self.duration} sec {"work" if self.isWork else "break"}'

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

        self.useMiniBreak: bool = False

        self.normalBreakDuration: int = 0
        self.normalWorkDuration: int = 0

        self.miniBreakDuration: int = 0
        self.miniWorkDuration: int = 0

        self.__timer: int = 0
        self.__items: List[History.Item] = []
        self.__nextBreakDuration: int = 0
        self.__unusedBreakDuration: int = 0

        self.__prevMiniBreak = 0

    def __repr__(self) -> str:
        return f'''
state: {'break' if self.inBreak else 'work'}
items: {self.__items}
next break duration: {self.nextBreakDuration} sec
timer: {self.__timer}
'''

    def clear(self):
        self.__items = []
        self.__nextBreakDuration = 0
        self.__unusedBreakDuration = 0
        self.__prevMiniBreak = 0
        self.timer = 0

    @property
    def working(self) -> bool:
        return len(self.__items) > 0 and self.__items[-1].isWork is True

    @property
    def lastWorkDuration(self) -> int:
        if self.working:
            return self.__items[-1].duration
        else:
            return self.__items[-2].duration if len(self.__items) >= 2 else 0

    @property
    def inBreak(self) -> bool:
        return len(self.__items) > 0 and self.__items[-1].isWork is False

    @property
    def lastBreakDuration(self) -> int:
        if self.inBreak:
            return self.__items[-1].duration
        else:
            return self.__items[-2].duration if len(self.__items) >= 2 else 0

    @property
    def nextBreakDuration(self) -> int:
        return self.__nextBreakDuration

    @property
    def timer(self) -> int:
        return self.__timer

    @timer.setter
    def timer(self, value) -> None:
        if self.__timer == value:
            return

        self.__timer = value
        self.timerChanged.emit(value)

    @property
    def isValid(self) -> bool:
        return (
            self.normalBreakDuration > 0 and
            self.normalWorkDuration > 0 and
            (
                (
                    self.useMiniBreak is True and
                    self.miniBreakDuration > 0 and
                    self.miniWorkDuration > 0
                ) or (
                    self.useMiniBreak is False
                )
            )
        )

    def updateLastDuration(self, duration: int) -> None:
        if len(self.__items) > 0:
            self.__items[-1].duration = duration
        else:
            raise RuntimeError('History is empty.')

        self.__updateCounters()

    def __addNewEntry(self, item: Item) -> None:
        self.__items.append(item)

    def startWork(self) -> None:
        self.__addNewEntry(History.Item(duration=0, isWork=True))
        self.__prevMiniBreak = 0

    def startBreak(self) -> None:
        self.__addNewEntry(History.Item(duration=0, isWork=False))

    def finishBreak(self) -> None:
        '''Mark current break as completely taken.'''
        if not self.inBreak:
            return

        self.__unusedBreakDuration = 0
        self.__items[-1].duration = self.__nextBreakDuration

    def postponeBreak(self) -> None:
        '''Adds remaining break to the next break.'''
        if not self.inBreak:
            return

        self.__unusedBreakDuration += max(0, self.nextBreakDuration - self.lastBreakDuration)

    def __updateCounters(self) -> None:
        if not self.isValid:
            return

        if self.inBreak:
            self.__updateBreakCounters()
        else:
            self.__updateMainCounters()
            if self.useMiniBreak:
                self.__updateMiniCounters()

    def __updateBreakCounters(self) -> None:
            self.timer = self.__unusedBreakDuration + self.__nextBreakDuration - self.lastBreakDuration
            self.__unusedBreakDuration = 0

    def __updateMainCounters(self) -> None:
        current_work_duration = self.lastWorkDuration
        work_duration = self.normalWorkDuration
        break_duration = self.normalBreakDuration

        self.timer = work_duration - current_work_duration

        self.__nextBreakDuration = self.__unusedBreakDuration

        if self.timer >= 0:
            self.__nextBreakDuration += break_duration
        else:
            total_work = sum(map(lambda x: x.duration, filter(lambda x: x.isWork, self.__items)))
            total_break = sum(map(lambda x: x.duration, filter(lambda x: not x.isWork, self.__items)))
            scheduled_break = total_work / work_duration
            self.__nextBreakDuration += int(round(scheduled_break * break_duration)) - total_break

    def __updateMiniCounters(self) -> None:
        current_work_duration = self.lastWorkDuration
        work_duration = self.miniWorkDuration

        next_mini_break = (current_work_duration // work_duration) * work_duration

        if next_mini_break > self.__prevMiniBreak:
            self.__prevMiniBreak = next_mini_break
            self.startMiniBreak.emit(self.miniBreakDuration)
