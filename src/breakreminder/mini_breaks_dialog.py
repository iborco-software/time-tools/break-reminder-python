from datetime import timedelta
from pathlib import Path
from time import perf_counter_ns

from humanize import precisedelta
from PySide2.QtGui import QPixmap, QBitmap, QColor, QPainter, QResizeEvent
from PySide2.QtCore import Qt, QRect, QPoint, QTimer
from PySide2.QtWidgets import QApplication, QDialog, QFrame, QHBoxLayout, QLabel, QProgressBar, QPushButton, QSizePolicy, QVBoxLayout, QWidget

from breakreminder.time import humanizedDuration


class MiniBreaksDialog(QDialog):
    def __init__(self, duration: int) -> None:
        super().__init__()
        self.setWindowFlag(Qt.FramelessWindowHint, True)

        self.duration = duration
        self.radius = 10

        button = QPushButton(self.tr('&Done'))
        # button.setFlat(True)
        button.setDefault(True)
        button.setSizePolicy(QSizePolicy(QSizePolicy.Preferred, QSizePolicy.MinimumExpanding))
        button.clicked.connect(self.accept)
        button.setStyleSheet('''QPushButton {
            border-style: none;
            padding: 0px 24px;
        }
        QPushButton:pressed {
            background-color: palette(mid);
        }
        ''')

        iconLabel = QLabel()
        iconLabel.setPixmap(QPixmap(str(Path(__file__).parent / 'resources' / 'mini-break.png')))
        iconLabel.setContentsMargins(8, 0, 0, 0)

        self.breakLabel = QLabel()
        self.breakLabel.setAlignment(Qt.AlignHCenter)

        self.breakLabel.setText('xxx')

        self.breakProgressBar = QProgressBar()
        self.breakProgressBar.setMaximum(self.duration)

        line = QFrame()
        line.setFrameShape(QFrame.VLine)
        line.setFrameShadow(QFrame.Sunken)

        midLayout = QVBoxLayout()
        midLayout.setContentsMargins(0, 0, 0, 0)
        midLayout.addStretch()
        midLayout.addWidget(self.breakLabel)
        midLayout.addWidget(self.breakProgressBar)
        midLayout.addStretch()

        buttonLayout = QHBoxLayout()
        buttonLayout.setSpacing(0)
        buttonLayout.addWidget(line)
        buttonLayout.addWidget(button)

        layout = QHBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(iconLabel)
        layout.addLayout(midLayout)
        layout.addLayout(buttonLayout)
        self.setLayout(layout)

        self.setFixedWidth(400)
        self.setFixedHeight(64) # without this the window will be taller than necessary

        desktop_rect = QApplication.screens()[0].geometry()
        top_right = desktop_rect.topRight()
        top_right += QPoint(-48, 64)
        self_rect = self.rect()
        self_rect.moveTopRight(top_right)
        self.setGeometry(self_rect)

        self.started = perf_counter_ns()

        timer = QTimer(self)
        timer.setInterval(250)
        timer.timeout.connect(self.onTimeout)
        timer.start()

        self.onTimeout()

    def resizeEvent(self, event: QResizeEvent) -> None:
        self.makeWidgetRounded(self, radius=self.radius)

    def makeWidgetRounded(self, widget: QWidget, radius: int) -> None:
        b = QBitmap(widget.size())
        b.fill(QColor(Qt.color0))

        rect = QRect(QPoint(0,0), widget.size())

        painter = QPainter(b)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setBrush(Qt.color1)
        painter.drawRoundedRect(rect, radius, radius, Qt.AbsoluteSize)
        painter.end()

        widget.setMask(b)

    def onTimeout(self) -> None:
        elapsed = (perf_counter_ns() - self.started) // 1_000_000_000
        remaining = self.duration - elapsed
        self.breakProgressBar.setValue(remaining)

        if remaining > 0:
            self.breakLabel.setText(humanizedDuration(remaining))
        elif remaining == 0:
            self.breakLabel.setText('Break finished!')
        else:
            self.accept()


if __name__ == '__main__':
    _ = QApplication([])

    dialog = MiniBreaksDialog(10)
    dialog.show()

    success = dialog.exec()
