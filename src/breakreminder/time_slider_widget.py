from typing import List
from PySide2.QtCore import Qt, Signal
from PySide2.QtWidgets import QLabel, QSlider, QVBoxLayout, QWidget

from breakreminder.time import humanizedDuration


class TimeSliderWidget(QWidget):
    __values: List[int] = list(range(1, 61))
    __value: int = __values[0]
    __prefix: str = ''

    valueChanged = Signal(int)

    def __init__(self, prefix: str, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

        self.__slider = QSlider(Qt.Horizontal)
        self.__slider.setMinimumWidth(300)
        self.__slider.valueChanged.connect(self.__onSliderValueChanged)
        self.__label = QLabel()

        layout = QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.__label)
        layout.addWidget(self.__slider)
        self.setLayout(layout)
        layout.setStretch(0, 0)
        layout.setStretch(1, 1)

        self.prefix = prefix

    @property
    def value(self) -> int:
        return self.__value

    @value.setter
    def value(self, val: int) -> None:
        try:
            index = self.__values.index(val)
        except:
            print(f"WARNING: couldn't set value to {val}, so setting it to {self.__values[0] if len(self.__values) > 0 else 'minimum'}")
            index = 0
            val = self.__values[0] if len(self.__values) > 0 else self.__slider.minimum()

        self.__value = val
        self.__slider.setValue(index)
        self.__updateLabel()
        self.valueChanged.emit(self.__value)

    @property
    def values(self) -> List[int]:
        return self.__values

    @values.setter
    def values(self, values: List[int]) -> None:
        if self.__values == values:
            return

        if len(values) < 2:
            print(f"WARNING: ignoring invalid values list {values}")
            return

        index = self.__values.index(self.__value) if len(self.__values) > 0 else 0

        self.__values = values
        self.__slider.setMinimum(0)
        self.__slider.setMaximum(len(self.__values) - 1)
        self.value = self.__values[index] if len(values) > index + 1 else self.values[-1]

    @property
    def prefix(self) -> str:
        return self.__prefix

    @prefix.setter
    def prefix(self, text) -> None:
        self.__prefix = text
        self.__updateLabel()

    def __updateLabel(self):
        self.__label.setText(f'{self.prefix} {humanizedDuration(self.value)}')

    def __onSliderValueChanged(self, value):
        if len(self.__values) > 0:
            self.value = self.__values[value]


if __name__ == '__main__':
    from PySide2.QtWidgets import QApplication, QVBoxLayout
    from breakreminder.settings import Settings

    app = QApplication([])

    slider1 = TimeSliderWidget('Normal Break for')
    slider1.values = Settings.Breaks.NORMAL_BREAK_VALUES

    slider2 = TimeSliderWidget('Every')
    slider2.values = Settings.Breaks.NORMAL_WORK_VALUES

    slider3 = TimeSliderWidget('Mini Break for')
    slider3.values = Settings.Breaks.MINI_BREAK_VALUES

    slider4 = TimeSliderWidget('Every')
    slider4.values = Settings.Breaks.MINI_WORK_VALUES

    window = QWidget()
    layout = QVBoxLayout()
    layout.addWidget(slider1)
    layout.addWidget(slider2)
    layout.addWidget(slider3)
    layout.addWidget(slider4)
    layout.addStretch()
    window.setLayout(layout)
    window.show()
    app.exec_()
