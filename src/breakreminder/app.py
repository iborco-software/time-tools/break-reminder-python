from pathlib import Path
from typing import Final, Optional

import AppKit
from PySide2.QtWidgets import QApplication
from rumps import App as RumpsApp
from rumps import clicked

from breakreminder.breaks_dialog import BreaksDialog
from breakreminder.controller import Controller
from breakreminder.mini_breaks_dialog import MiniBreaksDialog
from breakreminder.preferences_dialog import PreferencesDialog
from breakreminder.settings import Settings


START_MENU: Final[str] = 'Start'
RESET_MENU: Final[str] = 'Reset'
START_MENU_RUNNING: Final[str] = 'Stop'
PAUSE_MENU: Final[str] = 'Pause'
PAUSE_MENU_PAUSED: Final[str] = 'Continue'
PREFERENCES_MENU: Final[str] = 'Preferences'
BREAK_MENU: Final[str] = 'Take a break'

class App(RumpsApp):
    def __init__(self):
        super().__init__(
            type(self).__name__,
            menu=[
                START_MENU,
                RESET_MENU,
                PAUSE_MENU,
                BREAK_MENU,
                None,
                PREFERENCES_MENU,
                None,
            ])

        self.__late = False

        p = Path(__file__).parent / 'resources/default.png'
        self.icon = str(p)

        self.controller = Controller()
        self.controller.updated.connect(self.onControllerUpdated)
        self.controller.startMiniBreak.connect(self.onControllerStartMiniBreak)
        self.controller.readSettings()

        self.preferencesDialog: Optional[PreferencesDialog] = None
        self.breaksDialog: Optional[BreaksDialog] = None
        self.miniBreaksDialog: Optional[MiniBreaksDialog] = None

        # set dock icon
        icon_file = str(Path(__file__).parent / 'resources' / 'app-icon-default.png')
        self.appIconDefault = AppKit.NSImage.alloc().initByReferencingFile_(icon_file)
        icon_file = str(Path(__file__).parent / 'resources' / 'app-icon-late.png')
        self.appIconLate = AppKit.NSImage.alloc().initByReferencingFile_(icon_file)
        AppKit.NSApp.setApplicationIconImage_(self.appIconDefault)

        if Settings().app.trackOnStart:
            self.onStart(None)

    @clicked(START_MENU)
    def onStart(self, _):
        self.requestUserAttention(False)

        if self.controller.running:
            self.controller.stopRunning()
            self.menu[START_MENU].title = START_MENU
            self.menu[RESET_MENU].set_callback(None)
            self.menu[PAUSE_MENU].set_callback(None)
            self.menu[BREAK_MENU].set_callback(None)
        else:
            self.controller.startRunning()
            self.menu[START_MENU].title = START_MENU_RUNNING
            self.menu[RESET_MENU].set_callback(self.onReset)
            self.menu[PAUSE_MENU].set_callback(self.onPaused)
            self.menu[BREAK_MENU].set_callback(self.onBreak)

        self.menu[PAUSE_MENU].title = PAUSE_MENU

    def onReset(self, _):
        if self.controller.running:
            self.controller.stopRunning()
            self.controller.startRunning()

    def onPaused(self, _):
        self.requestUserAttention(False)

        if self.controller.paused:
            self.controller.continueWorking()
            self.menu[PAUSE_MENU].title = PAUSE_MENU
        else:
            self.controller.pauseWorking()
            self.menu[PAUSE_MENU].title = PAUSE_MENU_PAUSED

    @clicked(PREFERENCES_MENU)
    def onPreferences(self, _):
        if self.preferencesDialog:
            self.show(self.preferencesDialog)
            return

        self.preferencesDialog = PreferencesDialog()
        self.preferencesDialog.readSettings()
        self.preferencesDialog.finished.connect(self.onPreferencesDialogFinished)
        self.show(self.preferencesDialog)

    def onPreferencesDialogFinished(self, result) -> None:
        if not self.preferencesDialog:
            return

        if result == 1:
            self.controller.readSettings()

        self.preferencesDialog = None

    def onBreak(self, _):
        if self.breaksDialog:
            self.show(self.breaksDialog)
            return

        self.controller.startBreak()

        self.breaksDialog = BreaksDialog(self.controller.nextBreakDuration)
        self.controller.timerChanged.connect(self.breaksDialog.onTimerChanged)
        self.breaksDialog.finished.connect(self.onBreaksDialogFinished)
        self.show(self.breaksDialog)

    def onBreaksDialogFinished(self, result) -> None:
        if not self.breaksDialog:
            return

        if self.controller.historyTimer < 0 or result == 1:
            self.controller.finishBreak()
            self.controller.update()
            print('>>> break finished')
        else:
            self.controller.postponeBreak()
            self.controller.update()
            print('>>> break postponed')

        self.breaksDialog = None

    def show(self, window) -> None:
        # print(f'activating existing {window.windowTitle()}')
        window.show()
        window.raise_()
        QApplication.setActiveWindow(window)
        window.activateWindow()

    def onControllerUpdated(self, text: str, late: bool):
        self.title = text

        if late != self.__late:
            self.__late = late
            p = Path(__file__).parent / 'resources' / ('late.png' if late else 'default.png')
            self.icon = str(p)
            AppKit.NSApp.setApplicationIconImage_(self.appIconLate if late else self.appIconDefault)

            if self.__late and self.controller.working:
                self.requestUserAttention()

    def onControllerStartMiniBreak(self, duration: int) -> None:
        if self.miniBreaksDialog:
            self.show(self.miniBreaksDialog)
            return

        self.miniBreaksDialog = MiniBreaksDialog(duration)
        self.miniBreaksDialog.setModal(False)
        self.miniBreaksDialog.finished.connect(self.onMiniBreaksDialogFinished)
        self.show(self.miniBreaksDialog)

    def onMiniBreaksDialogFinished(self, result) -> None:
        if not self.miniBreaksDialog:
            return

        print('>>> mini breaks dialog closed with', result)

        self.miniBreaksDialog = None

    def requestUserAttention(self, value: bool = True) -> None:
        request_type = 0 # critical request
        app = AppKit.NSApp
        if value:
            app.requestUserAttention_(request_type)
        else:
            app.cancelUserAttentionRequest_(request_type)


def run() -> None:

    info = AppKit.NSBundle.mainBundle().infoDictionary()
    # info['LSBackgroundOnly'] = '1' # hide the dock icon
    info['CFBundleIdentifier'] = 'ro.borco.break-reminder-python'
    info['CFBundleName'] = 'Break Reminder' # set the text in status bar

    Settings.init()

    _ = QApplication([])

    rumps_app = App()
    rumps_app.run()


if __name__ == "__main__":
    run()
