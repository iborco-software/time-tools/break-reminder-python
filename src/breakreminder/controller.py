from time import perf_counter_ns

from PySide2.QtCore import QObject, QTimer, Signal

from breakreminder.time import compactDuration
from breakreminder.history import History
from breakreminder.settings import Settings

class Controller(QObject):

    updated = Signal(str, int)
    timerChanged = Signal(int)
    startMiniBreak = Signal(int)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.__running: bool = False
        self.__paused: bool = False
        self.__started: int = 0
        self.__message: str = ''
        self.__late: bool = False
        self.__elapsedBeforePause: int = 0

        self.__timer = QTimer()
        self.__timer.timeout.connect(self.update)
        self.__timer.setInterval(100)

        self.__history = History()
        self.__history.timerChanged.connect(self.timerChanged)
        self.__history.startMiniBreak.connect(self.startMiniBreak)

    @property
    def elapsed(self) -> int:
        if self.__running:
            if self.__paused:
                return self.__elapsedBeforePause
            else:
                return self.__currentElapse() + self.__elapsedBeforePause
        else:
            return 0

    @property
    def historyTimer(self) -> int:
        return self.__history.timer

    @property
    def inBreak(self) -> bool:
        return self.running and self.__history.inBreak

    @property
    def working(self) -> bool:
        return self.running and self.__history.working

    @property
    def message(self) -> str:
        return self.__message

    @property
    def late(self) -> bool:
        return self.__late

    @property
    def running(self) -> bool:
        return self.__running

    @property
    def paused(self) -> bool:
        return self.__paused

    @property
    def nextBreakDuration(self) -> int:
        return self.__history.nextBreakDuration

    def __currentElapse(self) -> int:
        return (perf_counter_ns() - self.__started) // 1_000_000_000

    def startRunning(self) -> None:
        self.__running = True
        self.__paused = False
        self.__started = perf_counter_ns()
        self.__elapsedBeforePause = 0

        self.__timer.start()

        print('controller: started')
        self.__history.clear()
        self.__history.startWork()
        self.update()

    def stopRunning(self) -> None:
        self.__running = False
        self.__paused = False
        self.__elapsedBeforePause = 0

        self.__timer.stop()
        self.__history.clear()

        print('controller: stopped')
        self.update()

    def pauseWorking(self) -> None:
        if self.__running:
            self.__elapsedBeforePause += self.__currentElapse()
            self.__paused = True
            print('controller: paused')
            self.__timer.stop()
            self.update()
        else:
            print('controller not working: pause request ignored')

    def continueWorking(self) -> None:
        if self.__running:
            print('controller: continued')
            self.__paused = False
            self.__started = perf_counter_ns()
            self.__timer.start()
            self.update()
        else:
            print('controller not working: continue request ignored')

    def readSettings(self):
        settings = Settings()

        print('controller: reading settings from', settings.fileName)

        self.__history.normalBreakDuration = settings.breaks.normalBreak
        self.__history.normalWorkDuration = settings.breaks.normalWork

        self.__history.useMiniBreak = settings.breaks.useMiniBreak
        self.__history.miniBreakDuration = settings.breaks.miniBreak
        self.__history.miniWorkDuration = settings.breaks.miniWork

    def __updateHistory(self) -> None:
        # print('>>> controller update history:', self.__history)
        if self.running:
            self.__history.updateLastDuration(self.elapsed)

    def __updateLocalMembers(self) -> None:
        self.__message = compactDuration(self.__history.timer)
        self.__late = (self.__history.timer < 0 and self.__history.working)
        if self.__running:
            self.updated.emit(self.__message, self.__late)
        else:
            self.updated.emit('', False)

    def update(self):
        self.__updateHistory()
        self.__updateLocalMembers()
        # print('>>> controller update:')
        # print('    elapsed before pause:', self.__elapsedBeforePause)
        # print('    elapsed:', self.elapsed)

    def startBreak(self) -> None:
        if self.inBreak:
            return
        print('>>> controller: start break')
        self.__history.startBreak()
        self.__started = perf_counter_ns()
        self.__elapsedBeforePause = 0

    def finishBreak(self) -> None:
        if not self.inBreak:
            return
        # self.__updateHistory()
        self.__history.finishBreak()
        self.__history.startWork()
        self.__started = perf_counter_ns()
        self.__elapsedBeforePause = 0
        print('>>> controller: finish break')

    def postponeBreak(self) -> None:
        if not self.inBreak:
            return

        # self.__updateHistory()
        self.__history.postponeBreak()
        self.__history.startWork()
        self.__started = perf_counter_ns()
        self.__elapsedBeforePause = 0
        print('>>> controller: postpone break')


if __name__ == '__main__':
    from breakreminder.app import run
    run()
