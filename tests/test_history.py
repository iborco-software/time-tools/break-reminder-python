import pytest
from typing import List, Tuple

from breakreminder.history import History


@pytest.mark.parametrize(
    "use_mini, mini_break, mini_work, normal_break, normal_work, expected_is_valid",
    [
        (False, 0, 0, 0, 900, False),
        (False, 0, 0, 100, 0, False),
        (False, 0, 0, 100, 900, True),
        (True, 0, 90, 100, 900, False),
        (True, 10, 0, 100, 900, False),
        (True, 10, 90, 0, 900, False),
        (True, 10, 90, 100, 0, False),
        (True, 10, 90, 100, 900, True),
    ]
)
def test_is_valid(
        use_mini: bool,
        mini_break: int,
        mini_work: int,
        normal_break: int,
        normal_work: int,
        expected_is_valid: bool,
    ):
    h = History()

    h.useMiniBreak = use_mini

    h.miniBreakDuration = mini_break
    h.miniWorkDuration = mini_work

    h.normalBreakDuration = normal_break
    h.normalWorkDuration = normal_work

    assert h.isValid is expected_is_valid


def test_by_default_neither_working_nor_in_break():
    h = History()

    assert h.inBreak is False
    assert h.working is False
    assert h.lastBreakDuration == 0
    assert h.lastWorkDuration == 0


def test_in_break_and_working_after_starting_work():
    h = History()

    h.startWork()

    assert h.inBreak is False
    assert h.working is True


def test_last_work_duration_after_updating_work():
    h = History()

    h.startWork()
    h.updateLastDuration(20)

    assert h.lastWorkDuration == 20


def test_in_break_and_working_after_start_break():
    h = History()

    h.startBreak()

    assert h.inBreak is True
    assert h.working is False


def test_last_break_duration_after_updating_break():
    h = History()

    h.startBreak()
    h.updateLastDuration(10)

    assert h.lastBreakDuration == 10


@pytest.mark.parametrize(
    "items, expected_timer, expected_next_break_duration",
    [
        (
            [
                (True, 10),
            ], 90, 10),
        (
            [
                (True, 100),
                (False, 10),
                (True, 20),
            ], 80, 10),
        (
            [
                (True, 100),
                (False, 10),
                (True, 99),
            ], 1, 10),
        (
            [
                (True, 100),
                (False, 10),
                (True, 100),
            ], 0, 10),
        (
            [
                (True, 100),
                (False, 10),
                (True, 101),
            ], -1, 10),
        (
            [
                (True, 100),
                (False, 10),
                (True, 110),
            ], -10, 11),
        (
            [
                (True, 100),
                (False, 10),
                (True, 120),
            ], -20, 12),
    ]
)
def test_next_break_duration_and_timer_when_working(
        items: List[Tuple[bool, int]],
        expected_timer: int,
        expected_next_break_duration: int,
    ):
    h = History()

    h.normalBreakDuration = 10
    h.normalWorkDuration = 100

    for is_work, duration in items:
        if is_work:
            h.startWork()
        else:
            h.startBreak()
        h.updateLastDuration(duration)

    assert h.nextBreakDuration == expected_next_break_duration
    assert h.timer == expected_timer


@pytest.mark.parametrize(
    "items, expected_timer, expected_next_break_duration",
    [
        (
            [
                (True, 100),
                (False, 0),
            ], 10, 10),
        (
            [
                (True, 100),
                (False, 1),
            ], 9, 10),
        (
            [
                (True, 100),
                (False, 9),
            ], 1, 10),
        (
            [
                (True, 100),
                (False, 10),
            ], 0, 10),
        (
            [
                (True, 100),
                (False, 11),
            ], -1, 10),
        (
            [
                (True, 100),
                (False, 10),
                (True, 20),
            ], 80, 10),
        (
            [
                (True, 100),
                (False, 10),
                (True, 120),
                (False, 0),
            ], 12, 12),
        (
            [
                (True, 100),
                (False, 10),
                (True, 120),
                (False, 11),
            ], 1, 12),
        (
            [
                (True, 100),
                (False, 10),
                (True, 120),
                (False, 12),
            ], 0, 12),
    ]
)
def test_next_break_duration_and_timer_when_in_break(
        items: List[Tuple[bool, int]],
        expected_timer: int,
        expected_next_break_duration: int,
    ):
    h = History()

    h.normalBreakDuration = 10
    h.normalWorkDuration = 100

    for is_work, duration in items:
        if is_work:
            h.startWork()
        else:
            h.startBreak()
        h.updateLastDuration(duration)

    assert h.nextBreakDuration == expected_next_break_duration
    assert h.timer == expected_timer


@pytest.mark.parametrize(
    "items",
    [
        ([
            (True, 100),
            (False, 0),
        ]),
        ([
            (True, 100),
            (False, 1),
        ]),
        ([
            (True, 100),
            (False, 9),
        ]),
        ([
            (True, 100),
            (False, 10),
        ]),
        ([
            (True, 100),
            (False, 11),
        ]),
        ([
            (True, 100),
            (False, 10),
            (True, 120),
            (False, 0),
        ]),
        ([
            (True, 100),
            (False, 10),
            (True, 120),
            (False, 11),
        ]),
        ([
            (True, 100),
            (False, 10),
            (True, 120),
            (False, 12),
        ]),
    ]
)
def test_finish_break(
        items: List[History.Item]
    ):
    h = History()

    h.normalBreakDuration = 10
    h.normalWorkDuration = 100

    for is_work, duration in items:
        if is_work:
            h.startWork()
        else:
            h.startBreak()
        h.updateLastDuration(duration)

    h.finishBreak()
    h.startWork()
    h.updateLastDuration(0)

    assert h.nextBreakDuration == 10
    assert h.timer == 100


@pytest.mark.parametrize(
    "items, expected_next_break_duration",
    [
        ([
            (True, 100),
            (False, 0),
        ], 20),
        ([
            (True, 100),
            (False, 1),
        ], 19),
        ([
            (True, 100),
            (False, 9),
        ], 11),
        ([
            (True, 100),
            (False, 10),
        ], 10),
        ([
            (True, 100),
            (False, 11),
        ], 10),
        ([
            (True, 100),
            (False, 10),
            (True, 120),
            (False, 0),
        ], 22),
        ([
            (True, 100),
            (False, 10),
            (True, 120),
            (False, 11),
        ], 11),
        ([
            (True, 100),
            (False, 10),
            (True, 120),
            (False, 12),
        ], 10),
    ]
)
def test_postpone_break(
        items: List[History.Item],
        expected_next_break_duration: int,
    ):
    h = History()

    h.normalBreakDuration = 10
    h.normalWorkDuration = 100

    for is_work, duration in items:
        if is_work:
            h.startWork()
        else:
            h.startBreak()
        h.updateLastDuration(duration)

    h.postponeBreak()
    h.startWork()
    h.updateLastDuration(0)

    assert h.nextBreakDuration == expected_next_break_duration
    assert h.timer == 100


@pytest.mark.parametrize(
    "items, expected_timer, expected_next_break_duration",
    [
        ([
            (True, 80),
            (False, 0),
        ], 10, 10),
        ([
            (True, 80),
            (False, 0),
            (True, 0),
        ], 100, 10),
        ([
            (True, 10),
            (False, 0),
            (True, 50),
            (False, 0),
            (True, 10),
            (False, 0),
        ], 10, 10),
        ([
            (True, 10),
            (False, 0),
            (True, 50),
            (False, 0),
            (True, 10),
            (False, 0),
            (True, 0),
        ], 100, 10),
    ]
)
def test_break_before_time(
        items: List[History.Item],
        expected_timer: int,
        expected_next_break_duration: int,
    ):
    h = History()

    h.normalBreakDuration = 10
    h.normalWorkDuration = 100

    for is_work, duration in items:
        if is_work:
            h.startWork()
            h.updateLastDuration(duration)
        else:
            h.startBreak()
            h.updateLastDuration(duration)
            h.finishBreak()

    assert h.timer == expected_timer
    assert h.nextBreakDuration == expected_next_break_duration
