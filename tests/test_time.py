import pytest

from breakreminder.time import compactDuration


@pytest.mark.parametrize(
    "seconds, expected_duration",
    [
        (90, '  1:30'),
        (0, '  0:00'),
        (-1, ' -0:01'),
    ]
)
def test_compactDuration(seconds, expected_duration):
    assert compactDuration(seconds) == expected_duration
