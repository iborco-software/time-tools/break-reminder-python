import pytest

from breakreminder.controller import Controller


def test_constructor():
    c = Controller()
    assert c.running is False
    assert c.paused is False


def test_starting():
    c = Controller()
    c.startRunning()

    assert c.running is True


def test_stoping_paused():
    c = Controller()
    c.startRunning()
    c.pauseWorking()
    c.stopRunning()

    assert c.paused is False


def test_pausing_stopped():
    c = Controller()
    c.pauseWorking()

    assert c.paused is False


def test_pausing_started():
    c = Controller()
    c.startRunning()
    c.pauseWorking()

    assert c.paused is True
