# Break Reminder

A simple break reminder for MacOS. Developed with [rumps](https://pypi.org/project/rumps/) and [pyside2](https://pypi.org/project/PySide2/).

![status](docs/status.png)

Use the preferences dialog to configure break duration and frequency. You can also enable the mini-breaks from here.

![preferences dialog](docs/preferences.png)

Start a normal break by clicking the *Take a break* menu.

![normal break dialog](docs/normal-break.png)

If mini-breaks are enabled, a mini-break pops out periodically.

![mini break dialog](docs/mini-break.png)

## Installation

```bash
# use specific python version
poetry env use 3.8.6

# install dependencies
poetry install

# get a shell
poetry shell

# IMPORTANT: setup must be run inside the poetry shell
poetry run python setup.py py2app -A
```

After installation, start the app by launching *dist/Break Reminder.app* from *Finder*.

### Note

For *py2app* to be able to find the *login-items* scripts, poetry must be configured to install the virtual environment in *.venv* bellow top code folder.

```bash
poetry config virtualenvs.create true
poetry config virtualenvs.in-project true
```
